import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SheduleComponent } from './modules/shedule/shedule.component';
import { FlightsComponent } from './modules/flights/flights.component';
import { AircraftComponent } from './modules/aircraft/aircraft.component';
import { MapsComponent } from './modules/maps/maps.component';
import { RoutesComponent } from './modules/routes/routes.component';
import { CompanyComponent } from './modules/company/company.component';
import { LogbookComponent } from './modules/logbook/logbook.component';
import { TrackAssistaintComponent } from './modules/track-assistaint/track-assistaint.component';
import { GeneralComponent } from './modules/general/general.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FvComponent } from './modules/shedule/fv/fv.component';
import { UuddComponent } from './modules/shedule/uudd/uudd.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgSelectModule } from '@ng-select/ng-select';
// HttpClient module for RESTful API
import { HttpClientModule } from '@angular/common/http';
import { Sppi6MapModule } from '@ng-ol-map/sppi6-map';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth-interceptor';

import {
  ENVIRONMENT_TOKEN,
  DEFAULT_SETTINGS_TOKEN,
  DEFAULT_USER_SETTINGS_TOKEN,
  initializeFactory,
  ConfigLoaderService,
  ConfigLoaderModule,
  STATE_PREFIX,
  StateManagerService,
  AbstractStateManager
} from '@ng-ol-map/config-loader';

import { environment } from '../environments/environment';
import * as settings  from '../assets/sppi-map/settings/settings.json';
import * as userSettings from '../assets/sppi-map/settings/userSettings.json';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AppComponent,
    SheduleComponent,
    FlightsComponent,
    AircraftComponent,
    MapsComponent,
    RoutesComponent,
    CompanyComponent,
    LogbookComponent,
    TrackAssistaintComponent,
    GeneralComponent,
    FvComponent,
    UuddComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    BrowserAnimationsModule,
    HttpClientModule,
    Sppi6MapModule,
    ConfigLoaderModule.forRoot(),
    TranslateModule.forRoot(),
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeFactory,
      deps: [ConfigLoaderService],
      multi: true,
    },
    { provide: ENVIRONMENT_TOKEN, useFactory: () => environment },
    { provide: DEFAULT_SETTINGS_TOKEN, useFactory: () => settings },
    { provide: DEFAULT_USER_SETTINGS_TOKEN, useFactory: () => userSettings },
    { provide: STATE_PREFIX , useValue: 'anpa' },
    {
      provide: AbstractStateManager,
      useClass: StateManagerService,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
