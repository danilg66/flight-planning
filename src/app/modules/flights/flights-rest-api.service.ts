import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { retry, catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FlightsRestApiService {

  constructor(private http: HttpClient) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getAirports(): Observable<any> {
    let parametrs = {
      func: "GetData_AN_Ard",
      par1: "B"
    }
    console.log(parametrs);
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getAcRegistrations(): Observable<any> {
    let parametrs = {
      func: "GetData_VSTypeFL",
    }
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getAcTypes(id): Observable<any> {
    let parametrs = {
      func: "GetData_VSFL",
      par1: id
    }
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getRoute(from, to): Observable<any> {
    let parametrs = {
      func: "GetData_AN_Flt_Get",
      par1: from,
      par2: to,
      par3: 0
    }
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getCruise(type) {
    let parametrs = {
      func: "GetData_SelectLRCM",
      par1: type
    }
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  getMaxFL(type, id) {
    let parametrs = {
      func: "GetData_SelectMaxFL",
      par1: type,
      par2: id
    }
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  calculate(flight) {
    flight.date = new Date(flight.date);
    console.log('flight', flight);

    let parametrs = {
      func: "GetData_DataCalculate",
      par1: flight.from,
      par2: flight.to,
      par3: flight.route,
      par4: "false",
      par5: "false",
      par6: "0",
      par7: flight.date.getDate().toString().padStart(2, '0') + '.' + (flight.date.getMonth()+1).toString().padStart(2, '0') + '.' + flight.date.getFullYear(), //"26.05.2022",
      par8: flight.date.getHours().toString().padStart(2, '0') + flight.date.getMinutes().toString().padStart(2, '0'), //"1200",
      par9: "false",
      par10: flight.aircraftId,
      par11: flight.aircraft,
      par12: flight.type,
      par13: "0",
      par14: flight.cruise,
      par15: flight.maxFl,
      par16: flight.fixFL
    }
    console.log('parametrs', parametrs);
    return this.http.post<any>(environment.apiFlightPlanningURL, JSON.stringify(parametrs), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    let errorDetail: any = null;
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorDetail = error.error;
      errorDetail.status = error.status;
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    if (errorDetail) {
      return throwError(errorDetail);
    } else {
      return throwError(errorMessage);
    }
  }
}
