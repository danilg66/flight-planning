import { Component, OnInit } from '@angular/core';
import { FlightsRestApiService } from './flights-rest-api.service';
import { AcRegistration, AcTypes, Cruise, FilterParams, Airport, Flight, Route, MaxFl, Calculate } from './flights.classes';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {

  public addTagFilterFlight: (name) => void;

  marks: Array<string> = [
    'gray',
    'red',
    'blue'
  ];
  sidebarOpened = true;

  asideType: string = 'worldMap';

  filterParams: FilterParams = new FilterParams();
  filterApply = false;
  showFilter = false;

  references = {
    tails: [{tail: 'VQ-BAT'}, {tail: 'VQ-BAR'}, {tail: 'VQ-BAD'}],
    airlines: [{iata: 'TRS', icao: 'GGFR', id: 1}, {iata: 'TGF', icao: 'OOHF', id: 2}, {iata: 'LOH', icao: 'GTTF', id: 3}],
    flights: [],
    arcrafts: []
  }

  filterLoadAnimation = {
    tails: false,
    airlines: false,
    flights: false,
    arcrafts: false
  };
  // Для хранения данных, введенных в поля в фильтре -
  // чтобы при потере фокуса они парсились и вставлялись в поле
  selectEnteredData = {
    tails: '',
    airlines: '',
    flights: '',
    arcrafts: '',
  };
  // Таймер обновления данных
  interval: any;

  /** Отображение **/
  // активное окно
  activeWindow = 'showFlights';
  // отображаемый график
  displayedChart = 'verticalProfile';
  /** Отображение **/

  airports: Array<Airport> = [];
  acRegistrations: Array<AcRegistration> = [];
  acTypes: Array<AcTypes> = [];
  cruises: Array<Cruise> = [];
  flight: Flight = new Flight();
  routes: Array<Route> = [];
  maxFl: Array<MaxFl> = [];
  calculateData: Array<Calculate> = [];

  constructor(public restApi: FlightsRestApiService) {
    this.addTagFilterFlight = (name) => this.addTagFilterFromArray(name, 'flights');
  }

  ngOnInit(): void {
    this.getAirports();
    this.getAcRegistrations();
  }

  async getAirports() {
    return this.restApi.getAirports().subscribe(data => {
      this.airports = data;
    });
  }

  changeAirport(from, to) {
    if (from && to) {
      return this.restApi.getRoute(from, to).subscribe(data => {
        this.routes = data;
      });
    }
  }

  async getAcRegistrations() {
    return this.restApi.getAcRegistrations().subscribe(data => {
      this.flight.type = null;
      this.acRegistrations = data;
    });
  }

  changeAcRegistration(ac) {
    console.log(ac);
    this.flight.aircraftId = ac.F150020_ID;
    this.flight.aircraft = ac.F150020_UseRLE;
    this.getAcTypes(ac.F150020_ID);
    this.getCruise(ac.F150020_UseRLE);
    this.getMaxFL(ac.F150020_UseRLE, ac.F150020_ID);
  }

  async getAcTypes(id) {
    return this.restApi.getAcTypes(id).subscribe(data => {
      this.acTypes = data;
    });
  }

  async getCruise(type) {
    return this.restApi.getCruise(type).subscribe(data => {
      this.cruises = data;
    });
  }

  async getMaxFL(type, id) {
    return this.restApi.getMaxFL(type, id).subscribe(data => {
      console.log('data', data);

      this.maxFl = data;
    });
  }

  async calculate() {
    return this.restApi.calculate(this.flight).subscribe(data => {
      console.log(data);
      this.calculateData = []; // = data.Calculate;
      data.Calculate.forEach(item => {
        this.calculateData.push(new Calculate(item));
      })
    });
  }

  saveFlight() {
    this.activeWindow = 'showFlights';
  }

  closeFlight() {
    this.activeWindow = 'showFlights';
  }

  computeFlight() {
    this.activeWindow = 'computeFlight';
    this.calculate();
  }

  addFlight() {
    this.activeWindow = 'addFlight';
  }

  editFlight() {
    this.activeWindow = 'addFlight';
  }

  openFilter() {
    this.showFilter = !this.showFilter;
  }

  filterSwitch() {
    this.filterApply = !this.filterApply;
  }

  /**
   * Функция поиска в выпадающим списке по нескольким параметрам
   * @param {string} term Строка для поиска введеня пользователем
   * @param {ReferanceAirline} item Элемент для поиска
   */
   airportsSelectSearch(term: string, item) {
    term = term.toLowerCase();
    return (item.F551070_IATA_Code && item.F551070_IATA_Code.toLowerCase().indexOf(term) > -1) ||
            (item.F551070_Cd_ICAO && item.F551070_Cd_ICAO.toLowerCase().indexOf(term) > -1) ||
            (item.F551070_ArdNm && item.F551070_ArdNm.toLowerCase().indexOf(term) > -1) ||
            (item.F551070_ArdCode && item.F551070_ArdCode.toLowerCase().indexOf(term) > -1);
  }

  clearFilterParametr(field: string, event) {
    // event.stopPropagation();
    if (field === 'date') {
      delete this.filterParams['start'];
      delete this.filterParams['finish'];
    } else {
      delete this.filterParams[field];
    }
  }

  getFieldString(array, ids, field): string {
    let names = [];
    array.filter(el => ids.includes(el.id.toString()));
    ids.forEach(element => {
      let name = this.getById(array, +element);
      names.push(name[field]);
    });
    return names.join(', ');
  }

  getById(array: any, id: number) {
    return array.find(el => el.id === id);
  }


  // Обработка значения, введенного в поле фильтра
  addTagFilterFromArray(names, key) {
    if (!names) {
      return;
    }

    // Разделение массива на элементы
    if (key === 'textValue') {
      names = [names];
    } else {
      names = names.split(' ');
    }

    if (key === 'aftnNumber') {
      const res = [];
      for (let i = 0; i <= names.length; i++) {
        if (names[i] && isNaN(+names[i]) && names[i + 1]) {
          res.push(names[i] + ' ' + names[i + 1]);
          i++;
        } else if (names[i]) {
          res.push(names[i]);
        }
      }
      names = res;
    }

    // Скрытие оригинальной строки ng-select, так как
    // не корректно отрабатывает добавление элементов.
    this.filterLoadAnimation[key] = true;
    if (names.length > 1) {
      // Добавление всех элементов, если выше проверки пройденны
      names.forEach(element => {
        if (element.length !== 0 && !this.filterParams[key].includes(element)) {
          this.filterParams[key] = [...this.filterParams[key], element.trim()];
        }
      });
    } else if (!this.filterParams[key].includes(names[0])) {
      // Если в массиве один элемент, использование стандартного метода добавления
      // единичной записи для ng-select
      this.filterParams[key] = [...this.filterParams[key], names[0].trim()];
    }
    this.interval = setTimeout(() => {
      this.filterLoadAnimation[key] = false;
    }, 250);
  }

  // При наборе в поле получатель фильтра записываем то, что набрал пользователь
  // в переменную, чтобы при потере фокуса вставлять это в поле
  onSearchFilterInput($event, key) {
    this.selectEnteredData[key] = $event.term.replace('\t', '');
  }

  // При очистке поля сбросим сохраненные значения
  onClearFilterInput(key) {
    this.selectEnteredData[key] = null;
  }

  // При удалении элемента удалим его из списка
  onClearTagFilterInput(event, key) {
    let target = event.label.toUpperCase();
    let arr = this.selectEnteredData[key].trim().replace(/\s{2,}/ig, ' ').toUpperCase().split(' ');
    if (arr.includes(target)) {
       arr.splice(arr.indexOf(target), 1);
       this.selectEnteredData[key] = arr.join(' ');
    }
  }

  parseDateTime(date: string, time: string): string {
    if (date && date + 'T' + time) {
      return new Date(date + 'T' + time).toISOString();
    }
    return null;
  }

  parseDateTimeUtc(date: string, time: string): string {
    if (date && date + 'T' + time) {
      let utcDate = Date.parse(date + 'T' + time + ':00.000Z');
      return new Date(utcDate).toISOString();
    }
    return null;
  }

}
