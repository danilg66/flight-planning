export class FilterParams {
  constructor() {
    this.flights = [];
    this.airlines = [];
    this.aircrafts = [],
      this.from = '';
    this.to = '';
    this.user = '';
    this.start = null;
    this.finish = null;
  }

  flights: Array<string>;
  airlines: Array<string>;
  aircrafts: Array<string>;
  from: '';
  to: '';
  user: '';
  start: string;
  finish: string;
}

export class Flight {
  id: number;
  from: string;
  to: string;
  route: string;
  type: string;
  date: Date;
  aircraftId: string;
  aircraft: string;
  cruise: string;
  maxFl: string;
  fixFL: string;
}

export class Airport {
  F551070_ArdCode: string;
  F551070_ArdNm: string;
  F551070_Cd_ICAO: string;
  F551070_IATA_Code: string;
  F551070_ID: string;
}

export class AcRegistration {
  F150020_ID: string;
  F150020_ID_VSTypeBook: string;
  F150020_UseRLE: string;
}

export class AcTypes {
  F150040_FabrDate: string;
  F150040_FabrNm: string;
  F150040_FabrState: string;
  F150040_FlBegDate: string;
  F150040_ID: string;
  F150040_ID_VSTypeBook: string;
  F150040_ID_VSTypeFL: string;
  F150040_ModelSert: string;
  F150040_OkArx: string;
  F150040_OkBalance: string;
  F150040_OkOther: string;
  F150040_OkRent: string;
  F150040_OkSE: string;
  F150040_RegNo: string;
  F150040_RegPref: string;
  F150040_SerNo: string;
}

export class Cruise {
  LRCM: string;
  Rmk: string;
}

export class Route {
  F551130_ArdCodeArr: string;
  F551130_ArdCodeDep: string;
  F551130_Dist: string;
  F551130_FltName: string;
  F551130_ID: string;
  F551130_ID_AN_User_ArdArr: string;
  F551130_ID_AN_User_ArdDep: string;
  F551130_LastDate: string;
  F551130_OkAlt: string;
  F551130_Rtube: string;
  F551130_SFLP: string;
}

export class MaxFl {
  FL: string;
  Hf: string;
  Hm: string;
}

export class Calculate {
  constructor(item) {
    for (const key in item) {
      if (Object.prototype.hasOwnProperty.call(item, key)) {
        this[key] = item[key] === -1 ? null : item[key];
      }
    }
  }
  GSP: number;
  LRCM: string;
  M: string;
  Npp: number;
  PntMark: string;
  Qub: number;
  Such: number;
  TAS: number;
  Tc: number;
  Tnar: string;
  Tuch: string;
  WU: number;
  WW: number;

// GSP: -1
// LRCM: "LRC"
// M: ""
// Npp: 1
// PntMark: "UUWW"
// Qub: -1
// Such: 132
// TAS: -1
// Tc: -55
// Tnar: ""
// Tuch: ""
// WU: 272
// WW: 29
}
