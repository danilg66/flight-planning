

export class FilterParams {
    constructor() {
        this.flights = [];
        this.airlines = [];
        this.aircrafts = [],
        this.from = '';
        this.to = '';
        this.user = '';
        this.start = null;
        this.finish = null;
    }

    flights: Array<string>;
    airlines: Array<string>;
    aircrafts: Array<string>;
    from: '';
    to: '';
    user: '';
    start: string;
    finish: string;
}

export class MapSettings {
  constructor(latitude?: number, longitude?: number) {
    this.latitude = latitude ?? 43.7540553;
    this.longitude = longitude ?? 39.5313382;
    this.zoom = 8;
    this.layers = {
      restrictedAirspace: false,
      airports: false,
      weather: false,
      windAndTemperature: false,
      jetStream: false
    }
  }

  latitude: number;
  longitude: number;
  zoom: number;
  layers: {
    restrictedAirspace: boolean,
    airports: boolean,
    weather: boolean,
    windAndTemperature: boolean,
    jetStream: boolean
  }
}

