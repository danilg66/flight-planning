import { Component, OnInit } from '@angular/core';
import { FilterParams, MapSettings } from './shedule.classes';

declare var ol: any;

@Component({
  selector: 'app-shedule',
  templateUrl: './shedule.component.html',
  styleUrls: ['./shedule.component.scss']
})
export class SheduleComponent implements OnInit {

  public addTagFilterFlight: (name) => void;

  geo = navigator.geolocation;

  marks: Array<string> = [
    'blue',
    'green',
    'orange',
    'pink',
    'purple'
  ];
  sidebarOpened = true;

  asideType: string = 'worldMap';
  modalRightType: string = 'worldMap';
  searchBy: string = 'identifier';

  filterParams: FilterParams = new FilterParams();
  filterApply = false;
  showFilter = false;

  references = {
    tails: [{tail: 'VQ-BAT'}, {tail: 'VQ-BAR'}, {tail: 'VQ-BAD'}],
    airlines: [{iata: 'TRS', icao: 'GGFR', id: 1}, {iata: 'TGF', icao: 'OOHF', id: 2}, {iata: 'LOH', icao: 'GTTF', id: 3}],
    flights: [],
    arcrafts: []
  }

  coordinates = {
    airports: [
      {
        iata: 'SVX',
        icao: 'USSS',
        name: 'Екатеринбург',
        latitude: 56.750160,
        longitude: 60.802138
      }, {
        iata: 'KGD',
        icao: 'UMKK',
        name: 'Калининград',
        latitude: 54.883042,
        longitude: 20.585820
      }, {
        iata: 'LED',
        icao: 'ULLI',
        name: 'Санкт-Петербург',
        latitude: 59.799800,
        longitude: 30.273000
      }]
  }

  filterLoadAnimation = {
    tails: false,
    airlines: false,
    flights: false,
    arcrafts: false
  };
  // Для хранения данных, введенных в поля в фильтре -
  // чтобы при потере фокуса они парсились и вставлялись в поле
  selectEnteredData = {
    tails: '',
    airlines: '',
    flights: '',
    arcrafts: '',
  };
  // Таймер обновления данных
  interval: any;

  modifers = ['Airports of departure', 'Airports of arrival', 'Airports'];

  activeWindow = 'showFlights';

  mapSettings: MapSettings = new MapSettings(43.7540553, 39.5313382);
  userLocation = {longitude: null, latitude: null};
  mapLayers = {
    airports: null,
  }

  map: any;

  constructor() {
    this.addTagFilterFlight = (name) => this.addTagFilterFromArray(name, 'flights');
  }

  ngOnInit() {
    this.getUserLocation();
  }

  getUserLocation() {
    navigator.geolocation.getCurrentPosition((pos) => {
      this.userLocation.latitude = pos.coords.latitude;
      this.userLocation.longitude = pos.coords.longitude;
    }, err => {
      console.warn(`ERROR(${err.code}): ${err.message}`);
    }, {enableHighAccuracy: true});
  }

  ngAfterViewInit() {
    this.drawMap();
  }

  drawMap() {
    var mousePositionControl = new ol.control.MousePosition({
      coordinateFormat: ol.coordinate.createStringXY(4),
      projection: 'EPSG:4326',
      // comment the following two lines to have the mouse position
      // be placed within the map.
      className: 'custom-mouse-position',
      target: document.getElementById('mouse-position'),
      undefinedHTML: '&nbsp;'
    });


    this.map = new ol.Map({
      target: 'map',
      controls: [/*ol.control.defaults({
        attributionOptions: {
          collapsible: false
        }
      }).extend([mousePositionControl])*/],
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM()
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([this.mapSettings.longitude, this.mapSettings.latitude]),
        zoom: this.mapSettings.zoom
      })
    });

    this.map.on('click', function (args) {
      console.log(args.coordinate);
      // var lonlat = ol.proj.transform(args.coordinate, 'EPSG:3857', 'EPSG:4326');
      // console.log(lonlat);

      // var lon = lonlat[0];
      // var lat = lonlat[1];
      // alert(`lat: ${lat} long: ${lon}`);
    });

    this.createLayer('airports');

    document.getElementById('zoomIn').addEventListener('click', this.zoomIn.bind(this));
    document.getElementById('zoomOut').addEventListener('click', this.zoomOut.bind(this));

  }

  removeMapListener() {
    document.getElementById('zoomIn').removeEventListener('click', this.zoomIn);
    document.getElementById('zoomOut').removeEventListener('click', this.zoomOut);
  }

  zoomIn() {
    if (this.mapSettings.zoom < 20) {
      this.mapSettings.zoom++;
    }
    this.map.getView().setZoom(this.mapSettings.zoom);
  }

  zoomOut() {
    if (this.mapSettings.zoom > 0) {
      this.mapSettings.zoom--;
    }
    this.map.getView().setZoom(this.mapSettings.zoom);
  }

  setPosition() {
    var view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([this.mapSettings.longitude, this.mapSettings.latitude]));
  }

  addMapPoint(latitude, longitude) {
    var vectorLayer = new ol.layer.Vector({
      source:new ol.source.Vector({
        features: [new ol.Feature({
              geometry: new ol.geom.Point(ol.proj.transform([parseFloat(longitude), parseFloat(latitude)], 'EPSG:4326', 'EPSG:3857')),
          }), new ol.Feature({
            geometry: new ol.geom.Point(ol.proj.transform([56.750160, 60.802138], 'EPSG:4326', 'EPSG:3857')),
        })]
      }),
      style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg"
        })
      })
    });

    // this.map.addLayer(vectorLayer);
    // this.map.removeLayer(vectorLayer);
  }

  toggleLayer(type: string) {
    console.log(this.mapLayers[type]);

    if (this.mapSettings.layers[type]) {
      this.map.removeLayer(this.mapLayers[type]);
    } else {
      this.map.addLayer(this.mapLayers[type]);
    }

    this.mapSettings.layers[type] = !this.mapSettings.layers[type];
  }

  createLayer(type) {
    let routes = this.coordinates[type].map(el =>
      new ol.Feature({
        geometry: new ol.geom.Point(ol.proj.transform([parseFloat(el.longitude), parseFloat(el.latitude)], 'EPSG:4326', 'EPSG:3857')),
      })
    );

    let source = new ol.source.Vector({
      features: routes
    });

    let style = new ol.style.Style({
      image: new ol.style.Icon({
        anchor: [0.5, 0.5],
        anchorXUnits: "fraction",
        anchorYUnits: "fraction",
        src: "https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg"
      })
    });

    this.mapLayers[type] = new ol.layer.Vector({
      source,
      style
    });
  }

  setUserPosition() {
    this.mapSettings.latitude = this.userLocation.latitude;
    this.mapSettings.longitude = this.userLocation.longitude;
    console.log(this.userLocation.latitude, this.userLocation.longitude);

    this.addMapPoint(this.userLocation.latitude, this.userLocation.longitude);
    this.setPosition();

    var points = [[this.coordinates.airports[0].longitude, this.coordinates.airports[0].latitude],
                  [this.coordinates.airports[1].longitude, this.coordinates.airports[1].latitude]];
    this.drawLine(points);

    this.drawCircle(points[0], 4000)

    //navigator.geolocation.getCurrentPosition(this.success.bind(this), this.error);
  }

  drawLine(coords) {
		var line = new ol.geom.LineString(coords).transform('EPSG:4326', 'EPSG:3857');

		// create the feature
		var feature = new ol.Feature({
			geometry: line,
			name: 'Line'
		});

		var lineStyle = new ol.style.Style({
			stroke: new ol.style.Stroke({
				color: '#ffcc33',
				width: 2
				//lineDash: [4,8]
			})
		});

		var source = new ol.source.Vector({
			features: [feature]
		});
		var vector = new ol.layer.Vector({
			source: source,
			style: [lineStyle]
		});
		this.map.addLayer(vector);
  }

  drawCircle(coords, radius) {
    var circle = new ol.geom.Circle(ol.proj.transform(coords, 'EPSG:4326', 'EPSG:3857'), radius);
    var CircleFeature = new ol.Feature(circle);

    var vectorSource = new ol.source.Vector({
      projection: 'EPSG:4326',
    });

    vectorSource.addFeatures([CircleFeature]);

    var layer = new ol.layer.Vector({
        source: vectorSource,
        style: [
        new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'blue',
                width: 3
            }),
            fill: new ol.style.Fill({
                color: 'rgba(0, 0, 255, 0.1)'
            })
        })]
    });
    this.map.addLayer(layer);
  }

  saveFlight() {
    this.activeWindow = 'showFlights';
  }

  closeFlight() {
    this.activeWindow = 'showFlights';
  }

  openFilter() {
    this.showFilter = !this.showFilter;
  }

  filterSwitch() {
    this.filterApply = !this.filterApply;
  }

  clearFilterParametr(field: string, event) {
    // event.stopPropagation();
    if (field === 'date') {
      delete this.filterParams['start'];
      delete this.filterParams['finish'];
    } else {
      delete this.filterParams[field];
    }
  }

  getFieldString(array, ids, field): string {
    let names = [];
    array.filter(el => ids.includes(el.id.toString()));
    ids.forEach(element => {
      let name = this.getById(array, +element);
      names.push(name[field]);
    });
    return names.join(', ');
  }

  getById(array: any, id: number) {
    return array.find(el => el.id === id);
  }


  // Обработка значения, введенного в поле фильтра
  addTagFilterFromArray(names, key) {
    if (!names) {
      return;
    }

    // Разделение массива на элементы
    if (key === 'textValue') {
      names = [names];
    } else {
      names = names.split(' ');
    }

    if (key === 'aftnNumber') {
      const res = [];
      for (let i = 0; i <= names.length; i++) {
        if (names[i] && isNaN(+names[i]) && names[i + 1]) {
          res.push(names[i] + ' ' + names[i + 1]);
          i++;
        } else if (names[i]) {
          res.push(names[i]);
        }
      }
      names = res;
    }

    // Скрытие оригинальной строки ng-select, так как
    // не корректно отрабатывает добавление элементов.
    this.filterLoadAnimation[key] = true;
    if (names.length > 1) {
      // Добавление всех элементов, если выше проверки пройденны
      names.forEach(element => {
        if (element.length !== 0 && !this.filterParams[key].includes(element)) {
          this.filterParams[key] = [...this.filterParams[key], element.trim()];
        }
      });
    } else if (!this.filterParams[key].includes(names[0])) {
      // Если в массиве один элемент, использование стандартного метода добавления
      // единичной записи для ng-select
      this.filterParams[key] = [...this.filterParams[key], names[0].trim()];
    }
    this.interval = setTimeout(() => {
      this.filterLoadAnimation[key] = false;
    }, 250);
  }

  // При наборе в поле получатель фильтра записываем то, что набрал пользователь
  // в переменную, чтобы при потере фокуса вставлять это в поле
  onSearchFilterInput($event, key) {
    this.selectEnteredData[key] = $event.term.replace('\t', '');
  }

  // При очистке поля сбросим сохраненные значения
  onClearFilterInput(key) {
    this.selectEnteredData[key] = null;
  }

  // При удалении элемента удалим его из списка
  onClearTagFilterInput(event, key) {
    let target = event.label.toUpperCase();
    let arr = this.selectEnteredData[key].trim().replace(/\s{2,}/ig, ' ').toUpperCase().split(' ');
    if (arr.includes(target)) {
       arr.splice(arr.indexOf(target), 1);
       this.selectEnteredData[key] = arr.join(' ');
    }
  }

  parseDateTime(date: string, time: string): string {
    if (date && date + 'T' + time) {
      return new Date(date + 'T' + time).toISOString();
    }
    return null;
  }

  parseDateTimeUtc(date: string, time: string): string {
    if (date && date + 'T' + time) {
      let utcDate = Date.parse(date + 'T' + time + ':00.000Z');
      return new Date(utcDate).toISOString();
    }
    return null;
  }

  selectViewAside(view) {
    if (view === 'worldMap') {
      this.removeMapListener();
    }
    this.asideType = view;
    if (view === 'worldMap') {
      this.drawMap();
    }
  }

  resetMapLayers() {
    this.mapSettings.layers = {
      restrictedAirspace: false,
      airports: false,
      weather: false,
      windAndTemperature: false,
      jetStream: false
    }
  }
}
