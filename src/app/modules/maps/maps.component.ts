import { Component, OnInit } from "@angular/core";
import {
  ApiService,
  BaseFplPlan,
  FplIntegrationService,
  MeteoFacade,
  Sppi6MapService,
  FplType,
  BaseSharPlan,
} from "@ng-ol-map/sppi6-map";
import { Observable } from "rxjs/Observable";
import "rxjs/add/observable/of";
import { TranslateService } from "@ngx-translate/core";
import { ThemeService } from '@monitorsoft/als-ui';
@Component({
  selector: "app-maps",
  templateUrl: "./maps.component.html",
  styleUrls: ["./maps.component.less"],
})
export class MapsComponent implements OnInit {
  initMap = false;
  constructor(
    private api: ApiService,
    private sppi6MapService: Sppi6MapService,
    private fplIntegrationService: FplIntegrationService,
    private meteoFacade: MeteoFacade,
    private translate: TranslateService,
    private theme:ThemeService
  ) {}

  ngOnInit(): void {
    // инитим переводы
    this.translate.setDefaultLang("ru");
    this.translate.use("ru");
     // подключаем тему (если без параметров то будет светлая)
    this.theme.setTheme("als-dark-theme")

    this.sppi6MapService.GetMapReadyEvent().subscribe((data) => {
      console.log("Map init: ", data);
      // Ожидание инициализации карты
      if (data) {
        // Вывод слоя погоды / нужен список слоев, где их брать?
        // console.log('Set Meteo OK');
        this.meteoFacade.ToggleMeteoLayerVisible("GAMET", true);
      }
    });
  }

  setFpl() {
    // Установка Маршрута
    console.log('set FPL');
    this.fplIntegrationService.setFpl({ type: FplType.FLIGHT, objects: [{Route: 'UUEE USSS'}]});
  }

  onWrapperResized(): void {
    this.api.execute("map", "GetMap")?.updateSize();
  }

  listApi() {
    console.log("Module list:");
    this.api.printModulesList();

    console.log("API list:");
    this.api.printApi();
  }

  setRoute() {
    let routeGeoJson = `
      "type": "FeatureCollection",
      "features": [{
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [40.97138889, 37.27722222, 11111]
              },
              "properties": {
                  "id": "1",
                  "order": "1",
                  "name": "DDEE",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:22:14Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [10.676253, 46.678224, 13311]
              },
              "properties": {
                  "id": "2",
                  "order": "2",
                  "name": "DDEE",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:22:15Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [13.032821, 47.077493, 7111]
              },
              "properties": {
                  "id": "1",
                  "order": "3",
                  "name": "DDEE",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:22:16Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [13.022821, 47.097493, 16111]
              },
              "properties": {
                  "id": "1",
                  "order": "4",
                  "name": "DDEE",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:22:17Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [50.97138889, 47.27722222, 2222]
              },
              "properties": {
                  "id": "2",
                  "order": "5",
                  "name": "YYYY",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:23:18Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [55.97138889, 57.27722222, 13333]
              },
              "properties": {
                  "id": "3",
                  "order": "6",
                  "name": "DDDD",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:25:41Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [155.97138889, 57.27722222, 7133]
              },
              "properties": {
                  "id": "4",
                  "order": "7",
                  "name": "DDDD",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:26:14Z"
              }
          },
          {
              "type": "Feature",
              "geometry": {
                  "type": "Point",
                  "coordinates": [155.97138889, -57.27722222, 7755]
              },
              "properties": {
                  "id": "5",
                  "order": "8",
                  "name": "DDDD",
                  "routeid": "MZ09",
                  "time": "2009-09-04T09:27:14Z"
              }
          }
      ],
      "crs": {
          "type": "name",
          "properties": {
              "name": "urn:ogc:def:crs:EPSG::4740",
              "id": "1f0e37b1-16b0-45ca-b25c-65fea4c75f8b",
              "routename": "ИмяМаршрута"
          }
      }
  }`;

    // TODO Ошибка при чтении параметра enabledModules
    // console.log('SetRoute exec:');
    // this.api.execute('routes', 'SetRoute', routeGeoJson);
  }
}
