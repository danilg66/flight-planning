import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AircraftComponent } from './modules/aircraft/aircraft.component';
import { CompanyComponent } from './modules/company/company.component';
import { FlightsComponent } from './modules/flights/flights.component';
import { GeneralComponent } from './modules/general/general.component';
import { LogbookComponent } from './modules/logbook/logbook.component';
import { MapsComponent } from './modules/maps/maps.component';
import { RoutesComponent } from './modules/routes/routes.component';
import { FvComponent } from './modules/shedule/fv/fv.component';
import { SheduleComponent } from './modules/shedule/shedule.component';
import { UuddComponent } from './modules/shedule/uudd/uudd.component';
import { TrackAssistaintComponent } from './modules/track-assistaint/track-assistaint.component';

const routes: Routes = [
  {
    path: 'shedule',
    component: SheduleComponent
  },
  {
    path: 'shedule',
    children: [
      { path: 'fv', component: FvComponent},
      { path: 'uudd', component: UuddComponent},
    ],
    component: SheduleComponent
  },
  {
    path: 'flights',
    component: FlightsComponent,
  },
  {
    path: 'aircraft',
    component: AircraftComponent,
  },
  {
    path: 'maps',
    component: MapsComponent,
  },
  {
    path: 'routes',
    component: RoutesComponent,
  },
  {
    path: 'company',
    component: CompanyComponent,
  },
  {
    path: 'logbook',
    component: LogbookComponent,
  },
  {
    path: 'track-assistaint',
    component: TrackAssistaintComponent,
  },
  {
    path: 'general',
    component: GeneralComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
