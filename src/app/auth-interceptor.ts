import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

@Injectable({
    providedIn: 'root',
  })
  export class AuthInterceptor implements HttpInterceptor {
    constructor() {
      sessionStorage.setItem('token', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJkZXZ1c2VyIiwiYXVkaWVuY2UiOiJ1bmtub3duIiwiY3JlYXRlZCI6MTY2MjM3ODc3OTg0NiwiZXhwIjoxNjYyOTgzNTc5fQ.NK3VU5QyPa3J7XlMUZreq2LIs_FSsDyl101sem0C6I9nTRCTwZ4pQayXwh3ET783X0j5nF_u3-ScCDp0t6YAEQ');
    }
  
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = sessionStorage.getItem('token')
      if (token) {
        req = req.clone({
          setHeaders: {
            Authorization: token,
          },
        });
      }
  
      return next.handle(req);
    }
  }
  