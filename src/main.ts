import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

declare var ol: any;
declare let window: any;
declare let global: any;

window['CESIUM_BASE_URL'] = '/assets/cesium';

declare var require: any;

const settings = require('./assets/sppi-map/settings/settings.json');

window.settings = window.settings || {};
window.settings = settings;

if (environment.production) {
  enableProdMode();
}

setTimeout(() => {
  platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err =>  console.log(err));
}, 1000)
