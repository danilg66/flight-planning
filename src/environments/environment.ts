// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiFlightPlanningURL: 'http://dev.msural.ru:6080',
  needDevTools: true,  
   configs: [
   { key: 'system', url: '/assets/sppi-map/settings/settings.json?v=' + new Date().getTime() },
   { key: 'userSettings', url: '/assets/sppi-map/settings/userSettings.json?v=' + new Date().getTime() },
   { key: 'baseMaps', url: '/assets/sppi-map/settings/base-map.json?v=' + new Date().getTime() },
   { key: 'aniPresets', url: '/assets/sppi-map/settings/aniConf.json?v=' + new Date().getTime() }
   ],  
   filtersUrl: '/assets/sppi-map/settings/filters.json?v=' + new Date().getTime(),
   filtersFirUrl: '/assets/sppi-map/settings/filters_fir.json?v=' + new Date().getTime(),  
   filtersUirUrl: '/assets/sppi-map/settings/filters_uir.json?v=' + new Date().getTime(),  
   filtersVisualUrl: '/assets/sppi-map/settings/filters_visual.json?v=' + new Date().getTime()
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
