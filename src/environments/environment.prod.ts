export const environment = {
  production: true,
  apiFlightPlanningURL: 'http://dev.msural.ru:6080',
  needDevTools: true,  
   configs: [
   { key: 'system', url: '/assets/sppi-map/settings/settings.json?v=' + new Date().getTime() },
   { key: 'userSettings', url: '/assets/sppi-map/settings/userSettings.json?v=' + new Date().getTime() },
   { key: 'baseMaps', url: '/assets/sppi-map/settings/base-map.json?v=' + new Date().getTime() }
   ],  
   filtersFirUrl: '/assets/sppi-map/settings/filters_fir.json?v=' + new Date().getTime(),  
   filtersUirUrl: '/assets/sppi-map/settings/filters_uir.json?v=' + new Date().getTime(),  
   filtersVisualUrl: '/assets/sppi-map/settings/filters_visual.json?v=' + new Date().getTime()
};
